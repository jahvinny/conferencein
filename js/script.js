var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
	   showGuestList(this);
	   
    }
  };
  xmlhttp.open("GET", "https://gist.githubusercontent.com/eMatsiyana/e315b60a2930bb79e869b37a6ddf8ef1/raw/10c057b39a4dccbe39d3151be78c686dcd1101aa/guestlist.xml", true);
  xmlhttp.overrideMimeType('text/xml');
  xmlhttp.send();
  
  function find_guest(event)
	{
		// create variables
		var sel_name,sel_surname;
		if(event.name !== "validation")
		{
							
			// Get the ul element
			var ulParent = document.getElementById("navigation_menu");

			// Get all li's with class="navigation_item" inside the ul element
			var li_child = ulParent.getElementsByClassName("navigation_item");

			// Loop through the li elements and add the active class to the current/clicked guest name
			for (var i = 0; i < li_child.length; i++) {
			  li_child[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");
				
				// check if there any li with active and remove class active
				 if (current.length > 0) { 
					 current[0].className = current[0].className.replace(" active", "");
				}

				// Add the active class to the current/clicked li element
				this.className += " active";
			  });
			}
			
			//get current clicked li and retrieve name and surname of the guest
			var list = document.getElementById(this.id);
			sel_name= list.getElementsByTagName('a')[0].innerHTML;
			sel_surname= list.getElementsByTagName('span')[0].innerHTML;
		}
		
		var xmlhttp = new XMLHttpRequest();
		  xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(event.name == "validation")// if button submit is clicked
				{
					addContacts(this);
				}else{
					//else if links clicked
			        getGuestInfo(this,sel_name,sel_surname);
				}
			}
		  };
		  xmlhttp.open("GET", "https://gist.githubusercontent.com/eMatsiyana/e315b60a2930bb79e869b37a6ddf8ef1/raw/10c057b39a4dccbe39d3151be78c686dcd1101aa/guestlist.xml", true);
		  xmlhttp.overrideMimeType('text/xml');
		  xmlhttp.send();
		
		

	}
  
  function showGuestList(xml) {

		var xmlDoc = xml.responseXML;

        var ul_tag = document.getElementById('navigation_menu');// The Parent Tag UL
        var record_list = xmlDoc.getElementsByTagName('record');  // find the xml record tag
		
		//disable email and cell number input fields after loading guest list
	   document.getElementById('guest_mail').disabled = true;
	   document.getElementById("guest_cell").disabled = true;
	   document.getElementById("btn_add").disabled = true;
		
		var total_tag = document.createElement('li');
		total_tag.id = 'guest_total';
		total_tag.innerHTML = record_list.length + ' Confirmed guests';
		ul_tag.appendChild(total_tag);
        for (var i = 0; i < record_list.length; i++) {
	
				
            // Create li tag and write first names
            var link_tag = document.createElement('li');
            link_tag.className = 'navigation_item';
			link_tag.id = "#"+i;
			link_tag.onclick = find_guest;
		
		
			var a_tag = document.createElement('a');
			a_tag.className = 'navigation_links';
			a_tag.href = "javascript:void(0)";
			
			
            a_tag.innerHTML =  record_list[i].getElementsByTagName("first_name")[0].childNodes[0].nodeValue;

			// Create span tag and write last names
            var span_tag = document.createElement('span');
            span_tag.className = 'move_surname';
            span_tag.innerHTML = record_list[i].getElementsByTagName("last_name")[0].childNodes[0].nodeValue;
			
			//Create span tag and write company names
            var span_company = document.createElement('span');
            span_company.className = 'move_company';
            span_company.innerHTML = record_list[i].getElementsByTagName("company")[0].childNodes[0].nodeValue;
			
			//Add child tags to li tag in order to have first name and last name in one line
			link_tag.appendChild(a_tag);
            link_tag.appendChild( span_tag);
            link_tag.appendChild( span_company);
			
		   
            // Add the child tag to UL tag.
            ul_tag.appendChild(link_tag);
			

        }
		
    };
	
	//Function for searching by first_name, last_name and company
	function searchFilter()
	{
			var input,filter,ul,li,a,i,searchValue; // create variables
			
			input = document.getElementById('search');
			filter = input.value.toUpperCase();
			ul = document.getElementById('navigation_menu');
			li = ul.getElementsByTagName('li');
			
			//Loop through all the guests, return matching information only
			for(i=0; i < li.length; i++)
			{
				
				a = li[i];
				searchValue = a.textContent || a.innerText;
				if (searchValue.toUpperCase().indexOf(filter) > -1) {
				  li[i].style.display = "";
				} else {
				  li[i].style.display = "none";
				}
			}
	}
	
	function getGuestInfo(xml,guestName,guestSurname)
	{
		var xmlDoc = xml.responseXML;

        var guest_name_tag = document.getElementById('guest_name');
        var guest_company_tag = document.getElementById('guest_company');
		
		//enabled email and cell input fields if user clicked on a specific guest
	   document.getElementById('guest_mail').disabled = false;
	   document.getElementById("guest_cell").disabled = false;
	   document.getElementById("btn_add").disabled = false;
	   
	   //reset the input values
	   document.getElementById('guest_mail').value = "";
	   document.getElementById('guest_cell').value = "";
		
        var record_list = xmlDoc.getElementsByTagName('record');  // find the xml record tag
		 for (var i = 0; i < record_list.length; i++) {
			 
			 //get first name and surname 
		     var get_name = record_list[i].getElementsByTagName("first_name")[0].childNodes[0].nodeValue;
		     var get_surname = record_list[i].getElementsByTagName("last_name")[0].childNodes[0].nodeValue;
			 
			 //find specific guest and if true return name, surname and company of the specific guest
			 if((guestName == get_name) && (guestSurname == get_surname))
			 {
				 //set guest information
				 guest_name_tag.innerHTML = get_name + ' ' + get_surname;
				 guest_company_tag.innerHTML = record_list[i].getElementsByTagName("company")[0].childNodes[0].nodeValue;
			 }
		
		 }
	}
	
	function addContacts(xml)
	{
		var email = document.getElementById('guest_mail').value;
		var cell_no = document.getElementById("guest_cell").value;
		
		//validate South African mobile number and correct email address format
		var regex_mobile = /^(\+?27|0)[6-8][0-9]{8}$/;
		var regex_email = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		
		if(regex_email.test(email) === false) //return alert message if email address is not valid
		{
			alert('Email address is not valid!');
			return false;
		}
		
		if(regex_mobile.test(cell_no) === false) //return alert message if mobile number is not valid
		{
			alert('Mobile number is not valid!');
			return false;
		}
		

		
		if((regex_email.test(email) === true) && (regex_mobile.test(cell_no) === true) )
		{
			var xmlDoc = xml.responseXML;
				
				// find guest information and store name and surname in different variables
			var guest_name_tag = document.getElementById('guest_name');
			var find_details = (guest_name_tag.textContent || guest_name_tag.innerText);
     		var space = find_details.indexOf(' ');
				
			var find_name = find_details.substring(0,space);
			var find_surname = find_details.substring(space,find_details.length);
				
	        var record_list = xmlDoc.getElementsByTagName('record');  // find the xml record tag
			for (var i = 0; i < record_list.length; i++) {
					 
				 //get first name and surname 
				 var get_name = record_list[i].getElementsByTagName("first_name")[0].childNodes[0].nodeValue;
				 var get_surname = record_list[i].getElementsByTagName("last_name")[0].childNodes[0].nodeValue;
					 
				 //find specific guest and if true return name, surname and company of the specific guest
				 if((find_name == get_name) && (find_surname == get_surname))
				 {
					 //create new element node
					 var emailElement = record_list[i].createElement('email_address');
					 var mobileElement = record_list[i].createElement('cell_no');
						 
					 //set attribute values
					var email_text =  record_list[i].createTextNode(email);
					var mobile_text =  record_list[i].createTextNode(cell_no);
					
					emailElement.appendChild(email_text);
					mobileElement.appendChild(mobile_text);

						 
					 record_list[i].getElementsByTagName("record")[0].appendChild(emailElement);
					 record_list[i].getElementsByTagName("record")[0].appendChild(mobileElement);
					 
					
					document.write(record_list[i].getElementsByTagName("email_address")[0].childNodes[0].nodeValue);
						 
				 }
				  document.forms['validation'].submit(); // submit form
			 }//End of for loop
			 
			 alert("Elements Added Successfully");
	
		}
	}

	
